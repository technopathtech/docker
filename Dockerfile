# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /var/www/
COPY ./django-app/requirements.txt /var/www/
RUN pip install -r requirements.txt